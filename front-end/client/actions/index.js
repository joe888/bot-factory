import Zjax from '../utils/zjax';
import ActionTypes from './ActionTypes';
import axios from 'axios';
var zjax = new Zjax();

// fetch robots
function fetchingRobotsPending() {
  return {
    type: ActionTypes.FETCHING_ROBOTS_PENDING,
  }
}

function fetchingRobotsRejected(err) {
  return {
    type: ActionsTypes.FETCHING_ROBOTS_REJECTED,
    err: err
  }
}


function receiveRobots(json) {
  return {
    type: ActionTypes.RECEIVE_ROBOTS,
    robots: json
  }
}


export const fetchRobots = (option) => {
  return function (dispatch) {
    if(option.has_more) {
      dispatch(fetchingRobotsPending());
      let page_num = option && option.page_num ? option.page_num : 1;
      let params = {
        per_page: 1000,
        page: page_num
      }
      zjax.request({
        url: '/robots',
        option: {
          method: 'get',
          params: params
        },
        successCallback: (response) => {
          dispatch(receiveRobots(response.data));
        },
        failureCallback: (err) => {
          dispatch(fetchingRobotsRejected(err));
        }
      });      
    }
  }
}

// robot batch processing
export const robotBatchPending = () => {
  return {
    type: ActionTypes.BATCH_PENDING
  }
}

function robotBatchFulfilled() {
  return {
    type: ActionTypes.BATCH_FULFILLED
  }
}

function robotBatchRejected() {
  return {
    type: ActionTypes.BATCH_REJECTED
  }
}


// extinguish robot
function extinguishRobotFulfilled(json) {
  return {
    type: ActionTypes.EXTINGUISH_ROBOT_FULFILLED,
    json: json
  }
}


function extinguishRobotRejected(err) {
  return {
    type: ActionTypes.EXTINGUISH_ROBOT_REJECTED,
    err: err
  }
}


export const extinguishProcessor = (ids) => {
  return function (dispatch) {
    dispatch(robotBatchPending());
    let requests = [];
    ids.forEach(id => {
      let _request = zjax.request({
        url: `/robots/${id}/extinguish`,
        option: {
          method: 'post'
        },
        successCallback: (response) => {
          dispatch(extinguishRobotFulfilled(response.data));
        },
        failureCallback: (err) => {
          dispatch(extinguishRobotRejected(err));
        }
      });
      requests.push(_request)
    });

    axios.all(requests)
      .then(axios.spread(function (acct, perms) {
        dispatch(robotBatchFulfilled());
      }));
  }
}

// recycle robot
function recycleRobotFulfilled(recycle_robot_ids) {
  return {
    type: ActionTypes.RECYCLE_ROBOT_FULFILLED,
    recycle_robot_ids: recycle_robot_ids
  }
}


function recycleRobotRejected(err) {
  return {
    type: ActionTypes.RECYCLE_ROBOT_REJECTED,
    err: err
  }
}

export const recycleProcessor = (recycle_robot_ids) => {
  return function (dispatch) {
    zjax.request({
      url: `/robots/recycle`,
      option: {
        method: 'post',
        params: {
          recycleRobots: recycle_robot_ids
        }
      },
      successCallback: (response) => {
        dispatch(recycleRobotFulfilled(recycle_robot_ids));
      },
      failureCallback: (err) => {
        dispatch(recycleRobotRejected(err));
      }
    })
  }
}


export const addToShipment = (id) => {
  return {
    type: ActionTypes.ADD_TO_SHIPMENT,
    robot_id: id
  }
}

export const removeFromShipment = (id) => {
  return {
    type: ActionTypes.REMOVE_FROM_SHIPMENT,
    robot_id: id
  }
}

// Shipment 
function sendShipmentPending() {
  return {
    type: ActionTypes.SEND_SHIPMENT_PENDING,
  }
}

function sendShipmentFulfilled() {
  return {
    type: ActionTypes.SEND_SHIPMENT_FULFILLED,
  }
}

function sendShipmentRejected(err) {
  return {
    type: ActionTypes.SEND_SHIPMENT_REJECTED,
    err: err
  }
}

export const sendShipment = (shipment_ids) => {
  return function (dispatch) {
    dispatch(sendShipmentPending());
    zjax.request({
      url: `/shipments/create`,
      option: {
        method: 'put',
        params: {
          ids: shipment_ids
        }
      },
      successCallback: (response) => {
        dispatch(sendShipmentFulfilled());
      },
      failureCallback: (err) => {
        dispatch(sendShipmentRejected(err));
      }
    })
  }  
}