import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import _ from 'lodash';

const dashboardPersistConfig = {
  key: 'DashboardReducer',
  storage,
  blacklist: ['robots', 'err', 'isFetchingRobots', 'isReceieveRobots', 'hasMore', 'page', 'factorySecondsRobots', 'passedQARobots', 'shippingRobots', 'isShippingRobots', 'isBatching']
}

let initState = {
	err: null,
  page: 1,
  robots: [],
  hasMore: true,
  isBatching: false,
  passedQARobots: [],
  shippingRobots: [],
	isFetchingRobots: false,
  isShippingRobots: false,
  factorySecondsRobots: []
}
const factorySecondFilter = (robot) => {
  let isFactorySecond = false;
  const {statuses} = robot.configuration;
  if(statuses.indexOf('rusty') !== -1 && (statuses.indexOf('loose screws') !== -1 || statuses.indexOf('paint scratched') !== -1)) {
    isFactorySecond = true;
  }
  return isFactorySecond
}

const dashboardReducer = (state = initState, action) => {
  var _robots = [].concat(state.robots),
      _factorySecondsRobots = [].concat(state.factorySecondsRobots),
      _passedQARobots = [].concat(state.passedQARobots),
      _shippingRobots = [].concat(state.shippingRobots),
      _shippedRobot = {};
  const stateWrapper = (nextState) => {
    return Object.assign({}, state, nextState)
  }
  switch (action.type) {
  	case 'FETCHING_ROBOTS_PENDING':
  		return stateWrapper({isFetchingRobots: true})
  	case 'FETCHING_ROBOTS_REJECTED':
  		return stateWrapper({isFetchingRobots: false, err: action.err})
  	case 'RECEIVE_ROBOTS':
      var nextPage = state.page + 1,
          hasMore = action.robots.length === 0 ? false : true;
  		return stateWrapper({
        hasMore: hasMore, 
        page: nextPage, 
        isFetchingRobots: false, 
        robots: [].concat(state.robots, action.robots)
      })
    case 'BATCH_PENDING':
      return stateWrapper({isBatching: true})
    case 'BATCH_FULFILLED':
      return stateWrapper({isBatching: false})
    case 'BATCH_REJECTED':
      return stateWrapper({isBatching: false})
    case 'EXTINGUISH_ROBOT_FULFILLED':
      var _robot = _robots.find(robot => robot.id === action.json.id);
      if (_robot) {
        _robot.configuration.statuses = action.json.configuration.statuses;
      }
      return stateWrapper({robots: _robots})
    case 'EXTINGUISH_ROBOT_REJECTED':
      return stateWrapper({err: action.err})
    case 'RECYCLE_ROBOT_FULFILLED':
      var _partionRobots = [];
      _robots = _robots.filter(robot => action.recycle_robot_ids.indexOf(robot.id) === -1);
      _partionRobots = _.partition(_robots, factorySecondFilter);
      return stateWrapper({
        isBatching: false, 
        robots: [], 
        factorySecondsRobots: [].concat(state.factorySecondsRobots, _partionRobots[0]), 
        passedQARobots: [].concat(state.passedQARobots, _partionRobots[1])
      })
    case 'RECYCLE_ROBOT_REJECTED':
      return stateWrapper({
        err: action.err
      })
    case 'ADD_TO_SHIPMENT':
      if(action.robot_id) {
        _shippedRobot = [].concat(_factorySecondsRobots, _passedQARobots).find(robot => robot.id === action.robot_id);
        _factorySecondsRobots = _factorySecondsRobots.filter(robot => robot.id !== action.robot_id);
        _passedQARobots = _passedQARobots.filter(robot => robot.id !== action.robot_id);
        _shippingRobots = _shippedRobot ? [].concat(_shippingRobots, _shippedRobot) : _shippingRobots;
      }
      return stateWrapper({
        factorySecondsRobots: _factorySecondsRobots, 
        passedQARobots: _passedQARobots, 
        shippingRobots: _shippingRobots
      })
    case 'REMOVE_FROM_SHIPMENT':
      if(action.robot_id) {
        _shippedRobot = _shippingRobots.find(robot => robot.id === action.robot_id);
        if(factorySecondFilter(_shippedRobot)) {
          _factorySecondsRobots = [].concat(_factorySecondsRobots, _shippedRobot);
        }else {
          _passedQARobots = [].concat(_passedQARobots, _shippedRobot);
        }
        _shippingRobots = _shippingRobots.filter(robot => robot.id !== _shippedRobot.id)
      }
      return stateWrapper({
        factorySecondsRobots: _factorySecondsRobots, 
        passedQARobots: _passedQARobots, 
        shippingRobots: _shippingRobots
      })
    case 'SEND_SHIPMENT_PENDING':
      return stateWrapper({
        isShippingRobots: true
      })
    case 'SEND_SHIPMENT_FULFILLED':
      return stateWrapper({
        isShippingRobots: false, 
        shippingRobots: []
      })
    case 'SEND_SHIPMENT_REJECTED':
      return stateWrapper({
        isShippingRobots: false,
        err: action.err
      })
    default:
      return state
  }
}

export default persistReducer(dashboardPersistConfig, dashboardReducer)