import React from 'react';
import Dashboard from '../components/Dashboard';
import {shallow, mount} from 'enzyme';
import renderer from 'react-test-renderer';
import robots from './fixtures/robots';

// snapshot test
test('>>> Dashboard test', () => {

  const component = renderer.create(
    <Dashboard robots={robots} factorySecondsRobots={[]} passedQARobots={[]} shippingRobots={[]} fetchRobots={()=>{}}/>,
  );
  let tree = component.toJSON();

  expect(tree).toMatchSnapshot();
});

describe('>>> Shallow Render Dashboard Component', () => {
  let extinguishProcessorReievedData = [],
      recycleProcessorReievedData = [],
      sendShipmentReievedData = [],
      fetchRobotsReievedData = {},
      wrapper = null,
      newState = {},
      initState = {
        robots: robots, 
        isFetchingRobots: false, 
        page: 1, 
        hasMore: true, 
        factorySecondsRobots: [], 
        passedQARobots: [], 
        shippingRobots: [], 
        addToShipment: () => {}, 
        removeFromShipment: () => {}, 
        sendShipment: (ids) => {
          sendShipmentReievedData = ids;
        }, 
        isBatching: false, 
        isShippingRobots: false, 
        robotBatchPending: () => {},
        extinguishProcessor: (ids) => {
          extinguishProcessorReievedData = ids;},
        recycleProcessor: (ids) => {recycleProcessorReievedData = ids;
        },
        fetchRobots: (option) => {
          fetchRobotsReievedData = option;
        },
        err: null
      };

  wrapper = mount(<Dashboard {...initState}/>)

  it('+++ render the component', () => {
    expect(wrapper.find(Dashboard).length).toEqual(1);
  });

  it('+++ load more robots', () => {
    wrapper.find('button#load-more-btn').simulate('click');
    expect(fetchRobotsReievedData).toEqual({
      page_num: 1,
      has_more: true
    });
  });

  it('+++ Q&A batching', () => {
    wrapper.find('button#qa-btn').simulate('click');
    expect(extinguishProcessorReievedData).toEqual([1, 2, 3, 4]);
    expect(recycleProcessorReievedData).toEqual([1, 3]);
  });

  newState = Object.assign({}, initState, {shippingRobots: robots})
  wrapper = mount(<Dashboard {...newState}/>)

  it('+++ Shipment', () => {
    wrapper.find('button#ship-btn').simulate('click');
    expect(sendShipmentReievedData).toEqual([1, 2, 3, 4]);
  });
});
