export default [{
  id: 1,
  name: 'Robot1',
  configuration: {
    hasSentience: true,
    hasWheels: true,
    hasTracks: true,
    numberOfRotors: 2,
    colour: "blue",
    statuses: ["paint scratched", "rusty", "loose screws"]
  }
}, {
  id: 2,
  name: 'Robot2',
  configuration: {
    hasSentience: true,
    hasWheels: false,
    hasTracks: true,
    numberOfRotors: 4,
    colour: "green",
    statuses: ["paint scratched", "rusty"]
  }
}, {
  id: 3,
  name: 'Robot3',
  configuration: {
    hasSentience: true,
    hasWheels: true,
    hasTracks: true,
    numberOfRotors: 7,
    colour: "red",
    statuses: ["on fire", "rusty", "loose screws"]
  }
}, {
  id: 4,
  name: 'Robot4',
  configuration: {
    hasSentience: true,
    hasWheels: false,
    hasTracks: false,
    numberOfRotors: 3,
    colour: "white",
    statuses: ["rusty"]
  }  
}];