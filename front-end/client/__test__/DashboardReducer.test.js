import React from 'react';
import DashboardReducer from '../reducers/DashboardReducer';
import ActionTypes from '../actions/ActionTypes';
import robots from './fixtures/robots';

describe('>>> DashboardReducer test', () => {
  it('+++ should return the initial state', () => {
    expect(DashboardReducer(undefined, {})).toEqual({
      err: null,
      factorySecondsRobots: [],
      hasMore: true,
      isBatching: false,
      isFetchingRobots: false,
      isShippingRobots: false,
      page: 1,
      passedQARobots: [],
      robots: [],
      shippingRobots: []    
    });
  });

  it('+++ should handle fetchingRobotsPending', () => {
    const _fetchPostPending = {
      type: ActionTypes.FETCHING_ROBOTS_PENDING
    };
    expect(DashboardReducer({}, _fetchPostPending)).toEqual({isFetchingRobots: true});
  });

  it('+++ should handle fetchingRobotsRejected', () => {
    const _fetchRobotsRejected = {
      type: ActionTypes.FETCHING_ROBOTS_REJECTED,
      err: 'err'
    };
    expect(DashboardReducer({}, _fetchRobotsRejected)).toEqual({
    	err: 'err',
    	isFetchingRobots: false,
    });
  });

  it('+++ should handle receiveRobots', () => {
    const _receiveRobots = {
      type: ActionTypes.RECEIVE_ROBOTS,
      robots: []
    };
    expect(DashboardReducer({
    	page: 1, 
    	robots: []
    }, _receiveRobots)).toEqual({
    	"hasMore": false, 
    	"isFetchingRobots": false, 
    	"page": 2, 
    	"robots": []
    });
  });

  it('+++ should handle robotBatchPending', () => {
    const _robotBatchPending = {
      type: ActionTypes.BATCH_PENDING
    };
    expect(DashboardReducer({}, _robotBatchPending)).toEqual({
    	isBatching: true
    });
  });  

  it('+++ should handle robotBatchFulfilled', () => {
    const _robotBatchFulfilled = {
      type: ActionTypes.BATCH_FULFILLED
    };
    expect(DashboardReducer({}, _robotBatchFulfilled)).toEqual({
    	isBatching: false
    });
  });  

  it('+++ should handle robotBatchRejected', () => {
    const _robotBatchRejected = {
      type: ActionTypes.BATCH_REJECTED
    };
    expect(DashboardReducer({}, _robotBatchRejected)).toEqual({
    	isBatching: false
    });
  }); 

  it('+++ should handle extinguishRobotFulfilled', () => {
    const _extinguishRobotFulfilled = {
      type: ActionTypes.EXTINGUISH_ROBOT_FULFILLED,
      json: {
      	id: 3,
      	configuration: {
      		statuses: ['on fire']
      	}
      }
    };
    expect(DashboardReducer({robots: [{
    	id: 3,
      configuration: {
      	statuses: []
      }
    }]}, _extinguishRobotFulfilled)).toEqual({robots: [{
    	id: 3,
      configuration: {
      	statuses: ['on fire']
      }
    }]});
  }); 

  it('+++ should handle extinguishRobotRejected', () => {
    const _extinguishRobotRejected = {
      type: ActionTypes.EXTINGUISH_ROBOT_REJECTED,
      err: 'error'
    };
    expect(DashboardReducer({}, _extinguishRobotRejected)).toEqual({
    	err: "error"
    });
  });

  it('+++ should handle recycleRobotFulfilled', () => {
    const _recycleRobotFulfilled = {
      type: ActionTypes.RECYCLE_ROBOT_FULFILLED,
      recycle_robot_ids: [1, 2]
    };
    expect(DashboardReducer({
    	robots: robots, 
    	factorySecondsRobots: [], 
    	passedQARobots: []
    }, _recycleRobotFulfilled)).toEqual({
    	isBatching: false,
    	robots: [],
    	factorySecondsRobots: [robots[2]],
    	passedQARobots: [robots[3]]
    });
  });

  it('+++ should handle recycleRobotRejected', () => {
    const _recycleRobotRejected = {
      type: ActionTypes.RECYCLE_ROBOT_REJECTED,
      err: 'error'
    };
    expect(DashboardReducer({}, _recycleRobotRejected)).toEqual({
    	err: 'error'
    });
  }); 

  it('+++ should handle addToShipment', () => {
    const _addToShipment = {
      type: ActionTypes.ADD_TO_SHIPMENT,
      robot_id: 3
    };
    expect(DashboardReducer({
    	shippingRobots: [], 
    	factorySecondsRobots: [robots[2]], 
    	passedQARobots: [robots[3]]
    }, _addToShipment)).toEqual({
    	factorySecondsRobots: [],
    	passedQARobots: [robots[3]],
    	shippingRobots: [robots[2]]
    });
  }); 

  it('+++ should handle removeFromShipment (factorySecondsRobots)', () => {
    const _removeFromShipment = {
      type: ActionTypes.REMOVE_FROM_SHIPMENT,
      robot_id: 3
    };
    expect(DashboardReducer({
    	shippingRobots: [robots[2], robots[3]], 
    	factorySecondsRobots: [], 
    	passedQARobots: []
    }, _removeFromShipment)).toEqual({
    	factorySecondsRobots: [robots[2]],
    	passedQARobots: [],
    	shippingRobots: [robots[3]]
    });
  });

  it('+++ should handle removeFromShipment (factorySecondsRobots)', () => {
    const _removeFromShipment = {
      type: ActionTypes.REMOVE_FROM_SHIPMENT,
      robot_id: 4
    };
    expect(DashboardReducer({
      shippingRobots: [robots[2], robots[3]], 
      factorySecondsRobots: [], 
      passedQARobots: []
    }, _removeFromShipment)).toEqual({
      factorySecondsRobots: [],
      passedQARobots: [robots[3]],
      shippingRobots: [robots[2]]
    });
  });

  it('+++ should handle sendShipmentPending', () => {
    const _sendShipmentPending = {
      type: ActionTypes.SEND_SHIPMENT_PENDING
    };
    expect(DashboardReducer({}, _sendShipmentPending)).toEqual({
    	isShippingRobots: true
    });
  });

  it('+++ should handle sendShipmentFulfilled', () => {
    const _sendShipmentFulfilled = {
      type: ActionTypes.SEND_SHIPMENT_FULFILLED
    };
    expect(DashboardReducer({shippingRobots: robots}, _sendShipmentFulfilled)).toEqual({
    	isShippingRobots: false,
    	shippingRobots: []
    });
  });

  it('+++ should handle sendShipmentRejected', () => {
    const _sendShipmentRejected = {
      type: ActionTypes.SEND_SHIPMENT_REJECTED,
      err: 'error'
    };
    expect(DashboardReducer({}, _sendShipmentRejected)).toEqual({
    	isShippingRobots: false,
      err: 'error'
    });
  });

});