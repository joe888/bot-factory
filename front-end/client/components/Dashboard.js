import React, { Component } from 'react';
import { 
  Button,
  Segment,
  Header,
  Table
} from 'semantic-ui-react';
import _ from 'lodash';

// render robot list
function RobotsList(props) {
  const { 
    robots, 
    enableRemoveBtn, 
    enableAddBtn, 
    addToShipment, 
    removeFromShipment
  } = props;
  let renderedTemplate = <Segment>No data</Segment>
  if (robots.length > 0) {
    renderedTemplate =  <Segment style={{height: '350px', overflowY: 'scroll'}}>
                          <Table celled padded>
                            <Table.Header>
                              <Table.Row>
                                <Table.HeaderCell singleLine>ID</Table.HeaderCell>
                                <Table.HeaderCell>Name</Table.HeaderCell>
                                <Table.HeaderCell>hasSentience</Table.HeaderCell>
                                <Table.HeaderCell>hasWheels</Table.HeaderCell>
                                <Table.HeaderCell>hasTracks</Table.HeaderCell>
                                <Table.HeaderCell>numberOfRotors</Table.HeaderCell>
                                <Table.HeaderCell>Colour</Table.HeaderCell>
                                <Table.HeaderCell>Statuses</Table.HeaderCell>
                                <Table.HeaderCell>Add To Shipment</Table.HeaderCell>
                                <Table.HeaderCell>Remove From Shipment</Table.HeaderCell>
                              </Table.Row>
                            </Table.Header>
                            <Table.Body>
                              {robots.map(robot => <RobotItem key={robot.id} robot={robot} enableRemoveBtn={enableRemoveBtn} enableAddBtn={enableAddBtn} addToShipment={addToShipment} removeFromShipment={removeFromShipment}/>)}
                            </Table.Body>
                          </Table>
                        </Segment>
  }
  return renderedTemplate
}

// robot item
function RobotItem(props) {
  const {
    robot, 
    enableRemoveBtn, 
    enableAddBtn, 
    addToShipment, 
    removeFromShipment
  } = props;
  let _statuses = robot.configuration.statuses.join(', ');
  let addActionCells = enableAddBtn ? <Table.Cell children={<Button className="add-ship-btn" onClick={()=>{addToShipment(robot.id)}}>Add</Button>}></Table.Cell> : <Table.HeaderCell></Table.HeaderCell>
  let removeActionCells = enableRemoveBtn ? <Table.Cell children={<Button className="rmv-ship-btn" onClick={()=>{removeFromShipment(robot.id)}}>Remove</Button>}></Table.Cell> : <Table.HeaderCell></Table.HeaderCell>
  return  <Table.Row className={`robot-${robot.id}`}>
            <Table.Cell>{robot.id}</Table.Cell>
            <Table.Cell>{robot.name}</Table.Cell>
            <Table.Cell>{robot.configuration.hasSentience ? 'true' : 'false'}</Table.Cell>
            <Table.Cell>{robot.configuration.hasWheels ? 'true' : 'false'}</Table.Cell>
            <Table.Cell>{robot.configuration.hasTracks ? 'true' : 'false'}</Table.Cell>
            <Table.Cell>{robot.configuration.numberOfRotors}</Table.Cell>
            <Table.Cell>{robot.configuration.colour}</Table.Cell>
            <Table.Cell>{_statuses}</Table.Cell>
            {addActionCells}
            {removeActionCells}
          </Table.Row>
}


class Dashboard extends Component {
	componentDidMount() {
    const {page, hasMore, fetchRobots} = this.props;
		fetchRobots({
      page_num: page,
      has_more: hasMore
    });
	}

  // robot recycling pipeline
  recycleRobotFilter(robot) {
    let needRecycled = false;
    const {colour, hasSentience, hasTracks, hasWheels, numberOfRotors, statuses} = robot.configuration;
    // condition a. Has fewer than 3 or greater than 8 rotors
    if(numberOfRotors < 3 || numberOfRotors > 8) {
      needRecycled = true;
    }
    // condition b. Has any number of rotors and blue in colour
    if(numberOfRotors > 0 && colour === 'blue') {
      needRecycled = true;
    }
    // condition c. Has both wheels and tracks
    if(hasWheels && hasTracks) {
      needRecycled = true;
    }
    // condition d. Has wheels and is rusty
    if(hasWheels && statuses.indexOf('rusty') !== -1) {
      needRecycled = true;
    }
    // condition e. Is sentient and has screws loose
    if(hasSentience && statuses.indexOf('loose screws') !== -1) {
      needRecycled = true;
    }
    // condition f. Is on fire
    if(statuses.indexOf('on fire') !== -1) {
      needRecycled = true;
    }
    return needRecycled
  }

  render() {
  	const {
      robots, 
      isFetchingRobots,
      page, 
      hasMore, 
      factorySecondsRobots, 
      passedQARobots, 
      shippingRobots, 
      addToShipment, 
      removeFromShipment, 
      sendShipment, 
      isBatching, 
      isShippingRobots, 
      robotBatchPending,
      extinguishProcessor,
      recycleProcessor,
      fetchRobots,
      err
    } = this.props;

    // simulate qa pipeline
    const qaProcessor = (robots) => {
      if(robots.length > 0) {
        extinguishProcessor(robots.map(robot => robot.id));
        recycleProcessor(robots.filter(robot => this.recycleRobotFilter(robot)).map(robot => robot.id));      
      }
    }

    // send shipment
    const sendClickHandler = () => {
      if(shippingRobots.length > 0) {
        sendShipment(shippingRobots.map(robot => robot.id));
      }
    }

    // load more robots
    const loadMoreClickHandler = () => {
      fetchRobots({
        page_num: page,
        has_more: hasMore
      });
    }

    // QA process click
    const qaProcessClickHandler = () => {
      qaProcessor(robots);
    }

    return (
      <div className="Dashboard">
        <Segment>
          <Header>All Robots ({robots.length})</Header>
          <RobotsList robots={robots} enableRemoveBtn={false} enableAddBtn={false} addToShipment={addToShipment} removeFromShipment={removeFromShipment}/>
          <Button id='load-more-btn' color='linkedin' disabled={isFetchingRobots || isBatching || isShippingRobots} loading={isFetchingRobots || isBatching} onClick={loadMoreClickHandler}>Load More Robots</Button>
          <Button id='qa-btn' color='linkedin' disabled={isFetchingRobots || isBatching || isShippingRobots} loading={isBatching} onClick={qaProcessClickHandler}>
            Q&A Process
          </Button>
        </Segment>
        <Segment>
          <Header>Factory Second Robots ({factorySecondsRobots.length})</Header>
          <RobotsList robots={factorySecondsRobots} enableRemoveBtn={false} enableAddBtn={true} addToShipment={addToShipment} removeFromShipment={removeFromShipment}/>
        </Segment>
        <Segment>
          <Header>Passed QA Robots ({passedQARobots.length})</Header>
          <RobotsList robots={passedQARobots} enableRemoveBtn={false} enableAddBtn={true} addToShipment={addToShipment} removeFromShipment={removeFromShipment}/>
        </Segment>
        <Segment>
          <Header>Ready to ship ({shippingRobots.length})</Header>
          <RobotsList robots={shippingRobots} enableRemoveBtn={true} enableAddBtn={false} addToShipment={addToShipment} removeFromShipment={removeFromShipment}/>
          <Button id='ship-btn' color='linkedin' disabled={isFetchingRobots || isBatching || isShippingRobots} loading={isShippingRobots} onClick={sendClickHandler}>
            Send shipment
          </Button>
        </Segment>
      </div>
    ) 
  }
}

export default Dashboard;
