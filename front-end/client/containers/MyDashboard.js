import { connect } from 'react-redux';
import { 
	addToShipment,
	removeFromShipment,
	sendShipment,
	robotBatchPending,
  fetchRobots,
  extinguishProcessor,
  recycleProcessor
} from '../actions';
import Dashboard from '../components/Dashboard'

const mapStateToProps = state => {
  return {
  	robots: state.DashboardReducer.robots,
  	isFetchingRobots: state.DashboardReducer.isFetchingRobots,
  	factorySecondsRobots: state.DashboardReducer.factorySecondsRobots,
  	passedQARobots: state.DashboardReducer.passedQARobots,
  	shippingRobots: state.DashboardReducer.shippingRobots,
  	err: state.DashboardReducer.err,
  	page: state.DashboardReducer.page,
  	hasMore: state.DashboardReducer.hasMore,
    isBatching: state.DashboardReducer.isBatching,
    isShippingRobots: state.DashboardReducer.isShippingRobots
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
    addToShipment: (id) => {
    	dispatch(addToShipment(id));
    },
    removeFromShipment: (id) => {
    	dispatch(removeFromShipment(id));
    },
    sendShipment: (ids) => {
    	dispatch(sendShipment(ids));
    },
    robotBatchPending: () => {
    	dispatch(robotBatchPending());
    },
    fetchRobots: (options) => {
      dispatch(fetchRobots(options));
    },
    extinguishProcessor: (ids) => {
      dispatch(extinguishProcessor(ids));
    },
    recycleProcessor: (ids) => {
      dispatch(recycleProcessor(ids));
    }
  }
}

const MyDashboard = connect(
  mapStateToProps,
  mapDispatchToProps
)(Dashboard);

export default MyDashboard;