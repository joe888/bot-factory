# bot-factory

## Installation

### Back-end
#### Requirements
* Ruby 2.4.x
* Rails 5.1

To install dependencies use bundle(ensure you are under back-end directory):

```sh
bundle
```

You can run rake task to generate robots

```sh
rake robot:generate_robots
```

Then start server:

```sh
rails s
```

#### Test

```sh
rspec spec
```


### Front-end
#### Requirements

* Node 6.12.x

To install dependencies use npm or yarn(ensure you are under front-end directory):
##### NPM
```sh
npm install
```

Then start server:

```sh
npm start
```

##### Yarn
```sh
yarn
```

Then start server:

```sh
yarn start
```

#### Test(Jest)
```sh
npm test
```
or
```sh
yarn test
```


## Roadmap

Based on the requirements of documents, the backend mainly provides basic manipulation of robots which stores in the database. Front-end app provides the access to enable the user to control the workflow through the UI elements interaction. So I decided to start from the back-end and then I will focus on the front-end.

### Architecture Diagram
![architecture_diagram](https://zymoridae.github.io/img/architecture_digram.png "Architecture Diagram")