require 'rails_helper'

RSpec.describe do
  # get robot
  describe 'GET /robots.json' do
    it 'should return all bots as a list' do
      robot1 = Robot.create(:name => 'robot1')
      robot2 = Robot.create(:name => 'robot2')
      get "/robots.json", {}
      response.should be_success
      result = JSON.parse response.body
      result.length.should eq 2
    end

    it 'should return empty list when records not found' do
      get "/robots.json", {}
      response.should be_success
      result = JSON.parse response.body
      result.length.should eq 0
    end

    it 'should return list based on pagination parameter' do
      robot1 = Robot.create(:name => 'robot1')
      robot2 = Robot.create(:name => 'robot2')
      robot3 = Robot.create(:name => 'robot3')
      get "/robots.json", {params: {page: 2, per_page: 2}}
      response.should be_success
      result = JSON.parse response.body
      result.length.should eq 1
      result[0]['id'].should eq robot3.id
    end

    it 'should only return robots not shipped' do
      robot1 = Robot.create(:name => 'robot1', is_shipped: true)
      robot2 = Robot.create(:name => 'robot2', is_shipped: true)
      robot3 = Robot.create(:name => 'robot3', is_shipped: false)
      get "/robots.json", {}
      response.should be_success
      result = JSON.parse response.body
      result.length.should eq 1
      result[0]['id'].should eq robot3.id
    end
  end

  # extinguish robot
  describe 'POST /robots/:id/extinguish.json' do
    it 'should return empty object when record not found' do
      robot = Robot.create(:name => 'robot')
      post "/robots/#{robot.id+1}/extinguish.json", {}
      response.should be_success
      result = JSON.parse response.body
      result['status'].should eq 'error'
      result['code'].should eq 422
      result['message'].should eq 'record not found'
    end

    it 'should return empty object when record statuses contains on fire status and has sentience' do
      robot = Robot.create(:name => 'robot', :statuses => ['on fire', 'rusty', 'paint scratched'], :has_sentience => true)
      post "/robots/#{robot.id}/extinguish.json", {}
      response.should be_success
      result = JSON.parse response.body
      result['configuration']['statuses'].should eq ['rusty', 'paint scratched']
    end

    it 'should return empty object when record statuses contains on fire status and has not sentience' do
      robot = Robot.create(:name => 'robot', :statuses => ['on fire', 'rusty', 'paint scratched'])
      post "/robots/#{robot.id}/extinguish.json", {}
      response.should be_success
      result = JSON.parse response.body
      result['configuration']['statuses'].should eq ['on fire', 'rusty', 'paint scratched']
    end


    it 'should return empty object when record statuses did not contain on fire status but has sentience' do
      robot = Robot.create(:name => 'robot', :statuses => ['rusty', 'paint scratched'], :has_sentience => true)
      post "/robots/#{robot.id}/extinguish.json", {}
      response.should be_success
      result = JSON.parse response.body
      result['configuration']['statuses'].should eq ['rusty', 'paint scratched']
    end 
  end

  # recycle robot
  describe 'POST /robots/recycle.json' do
    it 'should delete records based on the given recycleRobots ids' do
      [*1..3].each do |id|
        Robot.create(:name => "robot#{id}")
      end
      post "/robots/recycle.json", {params: {recycleRobots: Robot.all.map{|r| r.id}}}
      response.should be_success
      Robot.all.count.should eq 0
    end

    it 'should delete records based on the given recycleRobots ids if part of records not found' do
      [*1..3].each do |id|
        Robot.create(:name => "robot#{id}")
      end
      post "/robots/recycle.json", {params: {recycleRobots: Robot.all.map{|r| r.id}.concat([999999999])}}
      response.should be_success
      Robot.all.count.should eq 0
    end 
  end

  # create shipment
  describe 'POST /shipments/create' do
    it 'shoud create shipments based on the given ids' do
      robot1 = Robot.create(:name => 'robot1')
      robot2 = Robot.create(:name => 'robot2')
      robot3 = Robot.create(:name => 'robot3')
      put "/shipments/create", {params: {ids: [robot1.id, robot3.id]}}
      response.should be_success
      Robot.find(robot1.id).is_shipped.should eq true    
      Robot.find(robot3.id).is_shipped.should eq true    
    end

    it 'shoud create shipments based on the given ids if part of records not found' do
      robot1 = Robot.create(:name => 'robot1')
      robot2 = Robot.create(:name => 'robot2')
      robot3 = Robot.create(:name => 'robot3')
      put "/shipments/create", {params: {ids: [robot1.id, robot3.id].concat([999999999])}}
      response.should be_success
      Robot.find(robot1.id).is_shipped.should eq true    
      Robot.find(robot3.id).is_shipped.should eq true    
    end
  end

end
