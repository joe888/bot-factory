Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get  '/robots',                 to: 'robots#index',           as: :all_robots
  post '/robots/:id/extinguish',  to: 'robots#extinguish',      as: :robot_extinguish
  post '/robots/recycle',         to: 'robots#recycle',         as: :robot_recycle
  put  '/shipments/create',       to: 'robots#create_shipment', as: :robot_shipment
end
