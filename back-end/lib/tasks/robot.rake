namespace :robot do
  task :generate_robots, [:num] => :environment do |t, args|
    colour_list = ['red', 'green', 'blue', 'black', 'white']
    all_statuses = ['on fire', 'rusty', 'loose screws', 'paint scratched']
    Robot.destroy_all
    upper_range = args[:num].present? ? args[:num].to_i : 100
    [*1..upper_range].each do |index|
      p ">>> create robot#{index}"
      Robot.create(name: "Robot#{index}", has_sentience: rand(0..1) == 1 ? true : false, has_wheels: rand(0..1) == 1 ? true : false, has_tracks: rand(0..1) == 1 ? true : false, rotors_number: rand(0..15), colour: colour_list[rand(0..colour_list.length-1)], statuses: all_statuses.shuffle.first(rand(1..all_statuses.length)))
    end
  end
end