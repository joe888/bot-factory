class AddIsShipmentToRobots < ActiveRecord::Migration[5.1]
  def change
    add_column :robots, :is_shipped, :boolean, :default => false
  end
end
