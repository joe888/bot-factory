class CreateRobots < ActiveRecord::Migration[5.1]
  def change
    create_table :robots do |t|
      t.string      :name
      t.boolean     :has_sentience, default: false
      t.boolean     :has_wheels, default: false
      t.boolean     :has_tracks, default: false
      t.integer     :rotors_number, default: 0
      t.string      :colour, default: ''
      t.string      :statuses, default: [].to_yaml
      t.timestamps
    end
  end
end
