class Robot < ApplicationRecord
  serialize :statuses
  validates :name, :presence => true
end
