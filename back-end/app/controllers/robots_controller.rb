class RobotsController < ApplicationController
  protect_from_forgery with: :null_session
  # Returns a list of robots for QA
  def index
    page = params[:page].present? ? params[:page].to_i : 1    #default page 1
    per_page = 10                                             #deafult robot batch number 10
    if params[:per_page].present?
      per_page = params[:per_page].to_i
      per_page = per_page > 1000 ? 1000 : per_page 
    end
    @all_robots = Robot.where(is_shipped: false).page(page).per(per_page)
  end

  # Removes the `on fire` status from a robot
  def extinguish
    robot_id = params[:id]
    if robot_id.present?
      @robot = Robot.find robot_id
      if @robot.present? and @robot.has_sentience
        new_statuses = @robot.statuses.select{|status| status != 'on fire'}
        @robot.update_attributes statuses: new_statuses
      end
    end
  rescue ActiveRecord::RecordNotFound
    render json: {status: "error", code: 422, message: "record not found"}
  end

  # Accepts a list of IDs and removes the robots matching those IDs
  def recycle
    robot_ids = params[:recycleRobots]
    if robot_ids.present?
      Robot.where(:id => robot_ids.map{|robot_id| robot_id.to_i}).destroy_all
    end
  end

  # Create shipment
  def create_shipment
    robot_ids = params[:ids]
    if robot_ids.present?
      Robot.where(:id => robot_ids.map{|robot_id| robot_id.to_i}).update_all(is_shipped: true)
    end
  end
end
