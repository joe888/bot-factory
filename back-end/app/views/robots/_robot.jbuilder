json.id robot.id
json.name robot.name
json.configuration do
  json.hasSentience robot.has_sentience
  json.hasWheels  robot.has_wheels
  json.hasTracks  robot.has_tracks
  json.numberOfRotors robot.rotors_number
  json.colour robot.colour
  json.statuses robot.statuses
end
