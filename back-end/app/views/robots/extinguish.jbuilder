if @robot.present?
  json.partial! 'robot', robot: @robot
end