json.array! @all_robots do |robot|
  json.partial! 'robot', robot: robot
end